package com.rituo.lock;

import com.rituo.lock.utils.LockUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LockApplicationTests {

    @Resource
    private LockUtils lockUtils;
    @Test
    public void contextLoads() {
        lockUtils.decrStock();
    }

}
