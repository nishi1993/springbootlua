package com.rituo.lock.model;

import lombok.Data;

import java.util.List;

/**
 * @Author: sun
 * @Date: 2022/2/7 17:37
 */
@Data
public class Score {

    private Integer score;
}
