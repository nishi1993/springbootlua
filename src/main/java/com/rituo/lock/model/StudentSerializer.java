package com.rituo.lock.model;

import lombok.Data;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

/**
 * @Author: sun
 * @Date: 2022/2/7 17:37
 */
@Data
public class StudentSerializer<Student> implements RedisSerializer {

    private Integer age;

    private String name;

    @Override
    public byte[] serialize(Object o) throws SerializationException {
        return new byte[0];
    }

    @Override
    public Object deserialize(byte[] bytes) throws SerializationException {
        return null;
    }
}
