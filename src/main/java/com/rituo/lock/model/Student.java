package com.rituo.lock.model;

import lombok.Data;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import java.util.List;

/**
 * @Author: sun
 * @Date: 2022/2/7 17:37
 */
@Data
public class Student {

    private Integer age;

    private String name;

    private List<Score> list;

}
