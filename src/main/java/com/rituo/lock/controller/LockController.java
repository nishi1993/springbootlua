package com.rituo.lock.controller;

import com.rituo.lock.utils.LockUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Gjing
 **/
@RestController
public class LockController {
    @Autowired
    LockUtils lockUtils;

    @GetMapping("/lua")
    public ResponseEntity lua() throws Exception{
        return ResponseEntity.ok(lockUtils.lock());
    }

    public static void main(String[] args) {
        Integer i1 = 127;
        Integer i2 = 127;
        System.out.println(i1 == i2);

        Integer i3 = 128;
        Integer i4 = 128;
        System.out.println(i3 == i4);

        Integer i5 = -128;
        Integer i6 = -128;
        System.out.println(i5 == i6);

        Integer i7 = -129;
        Integer i8 = -129;
        System.out.println(i7 == i8);
    }
}