/**
 * runlion.com Inc.
 * Copyright (c) 2018-2019 All Rights Reserved.
 */
package com.rituo.lock;

import com.rituo.lock.model.Student;
import lombok.Data;
import org.springframework.data.redis.core.script.DefaultRedisScript;

/**
 *
 * @author lid
 * @date 2019-08-06 19:42
 */
@Data
public class RedisScript {

    private static RedisScript redisScript = new RedisScript();

    private RedisScript(){
    }

    public static RedisScript getInstance(){
        return redisScript;
    }

    public DefaultRedisScript<String>  lockScript;

    public DefaultRedisScript<Boolean> unlockScript;

    public DefaultRedisScript<Student> decrStockScript;
}
