package com.rituo.lock.LuaScriptBeanCfg;

import com.rituo.lock.RedisScript;
import com.rituo.lock.model.Student;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LuaLockConfiguration {

    /**
     * 向RedisScript塞入对应的Script
     * @return
     */
    @Bean
    public RedisScript redisScript() {
        RedisScript scripts = RedisScript.getInstance();
        //加锁
        DefaultRedisScript<String> lockScript = new DefaultRedisScript<>();
        lockScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("script/lock.lua")));
        lockScript.setResultType(String.class);
        scripts.setLockScript(lockScript);
        //释放锁
        DefaultRedisScript<Boolean> unlockScript = new DefaultRedisScript<>();
        unlockScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("script/unlock.lua")));
        unlockScript.setResultType(Boolean.class);
        scripts.setUnlockScript(unlockScript);
        //库存扣减
        DefaultRedisScript<Student> decrStockScript = new DefaultRedisScript<>();

        decrStockScript.setResultType(Student.class);
        decrStockScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("script/student.lua")));
        scripts.setDecrStockScript(decrStockScript);

        return scripts;
    }

}