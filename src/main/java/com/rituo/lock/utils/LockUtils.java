package com.rituo.lock.utils;

import com.rituo.lock.RedisScript;
import com.rituo.lock.model.Student;
import com.rituo.lock.model.StudentSerializer;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author lid
 * @date 2019-08-06 16:35
 */
@Component
public class LockUtils {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private RedisScript redisScript = RedisScript.getInstance();

    private static final Jackson2JsonRedisSerializer jackson2JsonRedisSerializerArgs= new Jackson2JsonRedisSerializer(Student.class);

    private static final Jackson2JsonRedisSerializer jackson2JsonRedisSerializerReturn= new Jackson2JsonRedisSerializer(Student.class);
    public Student lock() {
        return decrStock();
    }

    public Student decrStock() {
        List<String> keys = Arrays.asList("w", "y");
        Student student = new Student();
        student.setAge(1);
        Student[] list={student};
        String[] list0 = {"w", "y"};
        StopWatch stopWatch=new StopWatch("操作redis");

        stopWatch.start("第1次");
        stringRedisTemplate.execute(redisScript.lockScript,keys, list0);
        stopWatch.stop();

        stopWatch.start("第2次");
        Student result=stringRedisTemplate.execute(redisScript.decrStockScript,
                jackson2JsonRedisSerializerArgs,jackson2JsonRedisSerializerReturn,keys, list);
        System.out.println("学生打印"+result);
        stopWatch.stop();

        stopWatch.start("第3次");
        stringRedisTemplate.execute(redisScript.lockScript,keys, list0);
        stopWatch.stop();

        stopWatch.start("第4次");
        stringRedisTemplate.execute(redisScript.decrStockScript,
                jackson2JsonRedisSerializerArgs,jackson2JsonRedisSerializerReturn,keys, list);
        stopWatch.stop();

        stopWatch.start("第5次");
        stringRedisTemplate.execute(redisScript.lockScript,keys, list0);
        stopWatch.stop();

        System.out.println(stopWatch.toString());
        return result;
    }

}
